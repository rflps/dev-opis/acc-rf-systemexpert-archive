from org.csstudio.opibuilder.scriptUtil import PVUtil

# This script will populate several macros that will be used to mount the PV names inside the GUIs
# In this first version, the content of the macros are hardcoded inside this script. 
# The next step is to read the values from a file under version control;

# Section and subsection are fixed per script 
prefix_ = "MBL-020"
sec_ = "MBL"
subsec_ = "020"

# Adding the macros on the widget that will consume this script
widget.getPropertyValue("macros").add("PREFIX", prefix_)
widget.getPropertyValue("macros").add("SEC", sec_)
widget.getPropertyValue("macros").add("SUB", subsec_)

# This macro refers to the IOCStats Modules:
iocstats_ = "MBL-020"

##################################################################################################
#	Hardcoded macro data
##################################################################################################

# This macro refers to the IOC main PVs:
ioc_ = "RFS-FIM-110"

# Analog input macros
ai10_ = "RFS-Mod-110:Cur"
ai11_ = "RFS-Mod-110:Vol"
ai12_ = "RFS-SolPS-110:Cur"
ai13_ = "RFS-SolPS-120:Cur"
ai14_ = "RFS-SolPS-130:Cur"
ai15_ = "RFS-FIM-110:AI5"
ai16_ = "RFS-FIM-110:AI6"
ai17_ = "RFS-FIM-110:AI7"
ai18_ = "RFS-FIM-110:AI8"
ai19_ = "RFS-FIM-110:AI9"
ai00_ = "RFS-DirC-110:PwrFwdDA"
ai01_ = "RFS-DirC-110:PwrFwdPA"
ai02_ = "RFS-DirC-110:PwrFwdL2"
ai03_ = "RFS-DirC-110:PwrRflctPA"
ai04_ = "RFS-DirC-110:PwrFwdC1"
ai05_ = "RFS-DirC-110:PwrFwdC2"
ai06_ = "RFS-DirC-110:PwrFwdL1"
ai07_ = "RFS-DirC-110:PwrRflctL2"
ai08_ = "RFS-DirC-110:PwrRflctC1"
ai09_ = "RFS-DirC-110:PwrRflctC2"

di00_ = "RFS-SIM-110:HvEnaCmd"
di01_ = "RFS-SIM-110:RfEnaCmd"
di02_ = "RFS-SIM-110:PCconnect"
di03_ = "RFS-Mod-110:Fault"
di04_ = "RFS-VacPS-110:I-SP"
di05_ = "RFS-VacPS-120:I-SP"
di06_ = "RFS-ADR-110:ItlckStat"
di07_ = "RFS-FIM-110:DI7"
di08_ = "RFS-ADR-201001:ItckStat"
di09_ = "RFS-ADR-201002:ItckStat"
di10_ = "RFS-ADR-201003:ItckStat"
di11_ = "RFS-ADR-201004:ItckStat"
di12_ = "RFS-ADR-201005:ItckStat"
di13_ = "RFS-FIM-110:DI13"
di14_ = "RFS-FIM-110:DI14"
di15_ = "RFS-FIM-110:DI15"
di16_ = "RFS-FIM-110:DI16"
di17_ = "RFS-FIM-110:DI17"
di18_ = "RFS-FIM-110:DI18"
di19_ = "RFS-FIM-110:DI19"
di20_ = "RFS-FIM-110:DI20"

refpwr00_ = "RFS-FIM-110:REFPWR0"
refpwr01_ = "RFS-FIM-110:REFPWR1"

########################################################################################################

# Adding the macros on the widget that will consume this script
widget.getPropertyValue("macros").add("IOC_", ioc_)
widget.getPropertyValue("macros").add("IOCSTATS_", iocstats_)


# Analog input macros
widget.getPropertyValue("macros").add("AI00", ai00_)
widget.getPropertyValue("macros").add("AI01", ai01_)
widget.getPropertyValue("macros").add("AI02", ai02_)
widget.getPropertyValue("macros").add("AI03", ai03_)
widget.getPropertyValue("macros").add("AI04", ai04_)
widget.getPropertyValue("macros").add("AI05", ai05_)
widget.getPropertyValue("macros").add("AI06", ai06_)
widget.getPropertyValue("macros").add("AI07", ai07_)
widget.getPropertyValue("macros").add("AI08", ai08_)
widget.getPropertyValue("macros").add("AI09", ai09_)
widget.getPropertyValue("macros").add("AI10", ai10_)
widget.getPropertyValue("macros").add("AI11", ai11_)
widget.getPropertyValue("macros").add("AI12", ai12_)
widget.getPropertyValue("macros").add("AI13", ai13_)
widget.getPropertyValue("macros").add("AI14", ai14_)
widget.getPropertyValue("macros").add("AI15", ai15_)
widget.getPropertyValue("macros").add("AI16", ai16_)
widget.getPropertyValue("macros").add("AI17", ai17_)
widget.getPropertyValue("macros").add("AI18", ai18_)
widget.getPropertyValue("macros").add("AI19", ai19_)

# Digital input macros
widget.getPropertyValue("macros").add("DI00", di00_)
widget.getPropertyValue("macros").add("DI01", di01_)
widget.getPropertyValue("macros").add("DI02", di02_)
widget.getPropertyValue("macros").add("DI03", di03_)
widget.getPropertyValue("macros").add("DI04", di04_)
widget.getPropertyValue("macros").add("DI05", di05_)
widget.getPropertyValue("macros").add("DI06", di06_)
widget.getPropertyValue("macros").add("DI07", di07_)
widget.getPropertyValue("macros").add("DI08", di08_)
widget.getPropertyValue("macros").add("DI09", di09_)
widget.getPropertyValue("macros").add("DI10", di10_)
widget.getPropertyValue("macros").add("DI11", di11_)
widget.getPropertyValue("macros").add("DI12", di12_)
widget.getPropertyValue("macros").add("DI13", di13_)
widget.getPropertyValue("macros").add("DI14", di14_)
widget.getPropertyValue("macros").add("DI15", di15_)
widget.getPropertyValue("macros").add("DI16", di16_)
widget.getPropertyValue("macros").add("DI17", di17_)
widget.getPropertyValue("macros").add("DI18", di18_)
widget.getPropertyValue("macros").add("DI19", di19_)
widget.getPropertyValue("macros").add("DI20", di20_)

# Reflected Power macros
widget.getPropertyValue("macros").add("REFPWR0", refpwr00_)
widget.getPropertyValue("macros").add("REFPWR1", refpwr01_)

