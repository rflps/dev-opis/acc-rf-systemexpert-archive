

run = PVUtil.getString(pvs[0])

if run == '1':
    filename = PVUtil.getString(pvs[2]) # filename
    if (filename != ""):
        with open(filename) as csvfile:
            csvreader = csv.reader(csvfile)
            vals = list(csvreader)[0]
            res = []
            for v in vals:
                res.append(float(v))

        pvs[1].setValue(array(res, 'd'))
