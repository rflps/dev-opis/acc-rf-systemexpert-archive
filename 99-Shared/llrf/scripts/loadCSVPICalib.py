from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from jarray import array
import csv

run = PVUtil.getInt(pvs[0])
if run == 1:
    file_name = PVUtil.getString(pvs[1])
    rows1 = []
    rows2 = []
    with open(file_name) as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            rows1.append(float(row[0]))
            rows2.append(float(row[1]))

    pvs[2].setValue(array(rows1, 'd'))
    pvs[3].setValue(array(rows2, 'd'))
    pvs[0].setValue(0)

