from org.csstudio.display.builder.runtime.script import PVUtil
from jarray import array
import sys
import csv
from math import pi, sin

run = PVUtil.getString(pvs[0])

if run == '1':
    mode = PVUtil.getString(pvs[2]) # mode
    nelem = PVUtil.getInt(pvs[3]) # nelem
    arg2 = PVUtil.getDouble(pvs[4]) # arg2
    arg3 = PVUtil.getDouble(pvs[5]) # arg3
    filename = PVUtil.getString(pvs[6]) # filename

    if (mode == 'fixed'):
        val = arg2
        r = [0]
        for i in range(nelem-1):
            r.append(val)

    if (mode == 'ramp'):
        start = arg2
        stop = arg3
        step = (stop-start) / (nelem-1)

        r = [start]
        for i in range(nelem-1):
            r.append(r[-1] + step)

    if (mode == 'sin'):
        ampl = arg2
        freq = arg3
        t = list(range(nelem))

        i = 0
        r = []
        for val in t:
            r.append(ampl * sin(2 * pi * freq * val / float(nelem)))
        r[0] = 0.0

    pvs[1].setValue(array(r, 'd'))

    # save file
    if (filename != "") :
        with open(filename, "w") as csvfile :
            writer = csv.writer(csvfile)
            writer.writerow(r)
