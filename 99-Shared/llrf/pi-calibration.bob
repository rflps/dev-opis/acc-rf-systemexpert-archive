<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>PI Calibration</name>
  <width>530</width>
  <height>560</height>
  <widget type="rectangle" version="2.0.0">
    <name>Titlebar</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>530</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <class>TITLE</class>
    <text>$(PI_TYPE) Calibration</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>470</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="group" version="2.0.0">
    <name>MGGrey03</name>
    <y>50</y>
    <width>530</width>
    <height>510</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <widget type="rectangle" version="2.0.0">
      <name>MGGrey03-background_1</name>
      <width>530</width>
      <height>510</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="xyplot" version="2.0.0">
      <name>X/Y Plot</name>
      <x>40</x>
      <y>40</y>
      <width>470</width>
      <height>340</height>
      <title>Calibration Values</title>
      <show_legend>false</show_legend>
      <tooltip></tooltip>
      <x_axis>
        <title>Raw Values</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>100.0</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </x_axis>
      <y_axes>
        <y_axis>
          <title>EGU Values</title>
          <autoscale>true</autoscale>
          <log_scale>false</log_scale>
          <minimum>-10.0</minimum>
          <maximum>10.0</maximum>
          <show_grid>false</show_grid>
          <title_font>
            <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
            </font>
          </title_font>
          <scale_font>
            <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
            </font>
          </scale_font>
          <visible>true</visible>
        </y_axis>
      </y_axes>
      <traces>
        <trace>
          <name>Calibration Values</name>
          <x_pv>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE)CalRaw</x_pv>
          <y_pv>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE)CalEGU</y_pv>
          <err_pv></err_pv>
          <axis>0</axis>
          <trace_type>1</trace_type>
          <color>
            <color red="0" green="0" blue="255">
            </color>
          </color>
          <line_width>1</line_width>
          <line_style>0</line_style>
          <point_type>0</point_type>
          <point_size>10</point_size>
          <visible>true</visible>
        </trace>
      </traces>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_3</name>
      <text>Linear</text>
      <x>40</x>
      <y>390</y>
      <width>160</width>
    </widget>
    <widget type="slide_button" version="2.0.0">
      <name>Slide Button</name>
      <pv_name>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE)CalLin</pv_name>
      <label></label>
      <x>110</x>
      <y>390</y>
      <width>80</width>
    </widget>
    <widget type="slide_button" version="2.0.0">
      <name>Slide Button_1</name>
      <pv_name>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE)CalEn</pv_name>
      <label></label>
      <x>110</x>
      <y>420</y>
      <width>80</width>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_4</name>
      <text>Enable</text>
      <x>40</x>
      <y>420</y>
      <width>60</width>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_2</name>
      <pv_name>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=SP)CalStat</pv_name>
      <x>120</x>
      <y>460</y>
      <off_color>
        <color name="OK" red="61" green="216" blue="61">
        </color>
      </off_color>
      <on_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_66</name>
      <text>Status</text>
      <x>40</x>
      <y>460</y>
      <width>60</width>
      <height>30</height>
    </widget>
    <widget type="fileselector" version="2.0.0">
      <name>File Selector</name>
      <pv_name>loc://$(PD=LLRF:)$(RD=DIG01:)$(PI_TYPE)_filename("")</pv_name>
      <x>470</x>
      <y>400</y>
      <height>40</height>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>Text Entry</name>
      <pv_name>loc://$(PD=LLRF:)$(RD=DIG01:)$(PI_TYPE)_filename("")</pv_name>
      <x>300</x>
      <y>400</y>
      <width>170</width>
      <height>40</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_67</name>
      <text>FIle name:</text>
      <x>220</x>
      <y>410</y>
      <height>30</height>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_2</name>
      <actions>
        <action type="write_pv">
          <pv_name>loc://$(PD=LLRF:)$(RD=DIG01:)$(PI_TYPE)_save_calib(0)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <text>Save calibration tables</text>
      <x>220</x>
      <y>450</y>
      <width>110</width>
      <height>50</height>
      <scripts>
        <script file="scripts/saveCSVPICalib.py">
          <pv_name>loc://$(PD=LLRF:)$(RD=DIG01:)$(PI_TYPE)_save_calib(0)</pv_name>
          <pv_name trigger="false">loc://$(PD=LLRF:)$(RD=DIG01:)$(PI_TYPE)_filename("")</pv_name>
          <pv_name trigger="false">$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE)CalEGU</pv_name>
          <pv_name trigger="false">$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE)CalRaw</pv_name>
        </script>
      </scripts>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_3</name>
      <actions>
        <action type="write_pv">
          <pv_name>loc://$(PD=LLRF:)$(RD=DIG01:)$(PI_TYPE)_load_calib(0)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <text>Load calibration tables</text>
      <x>400</x>
      <y>450</y>
      <width>110</width>
      <height>50</height>
      <scripts>
        <script file="scripts/loadCSVPICalib.py">
          <pv_name>loc://$(PD=LLRF:)$(RD=DIG01:)$(PI_TYPE)_load_calib(0)</pv_name>
          <pv_name trigger="false">loc://$(PD=LLRF:)$(RD=DIG01:)$(PI_TYPE)_filename("")</pv_name>
          <pv_name trigger="false">$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE)CalEGU</pv_name>
          <pv_name trigger="false">$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE)CalRaw</pv_name>
        </script>
      </scripts>
      <tooltip>$(actions)</tooltip>
    </widget>
  </widget>
</display>
